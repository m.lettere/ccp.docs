.. CCP Documentation documentation master file, created by
   sphinx-quickstart on Thu Apr 27 09:47:54 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CCP's documentation!
=============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   quickstart/index
   methods_development/index
   architecture
   usermanual/index
   developermanual/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
