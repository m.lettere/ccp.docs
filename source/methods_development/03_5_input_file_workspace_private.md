# Input: Workspace File Selection

The `workspace` input type allows users to work with their private files stored in their personal workspace. When selecting an input of type `workspace`
![Input workspace](../_static/imgs/methods_development/input_workspace.png)

When selecting an input of type `workspace`, the interface allows users to browse their workspace and select the desired file during method configuration.

![workspace select file](../_static/imgs/methods_development/workspace_select_file.png)

method json

```json
{
    "inputs": {
        "inputFile": {
            "id": "inputFile",
            "title": "inputFile",
            "description": "Input CSV file()",
            "minOccurs": 1,
            "maxOccurs": 1,
            "schema": {
                "type": "string",
                "format": "remotefile",
                "default": "",
                "contentMediaType": "text/csv"
            }
        }
    },
    "additionalParameters": {
        "parameters": [
            {
                "name": "deploy-script",
                "value": [
                    "./download.sh {{inputFile}}"
                ]
            },
            {
                "name": "execute-script",
                "value": [
                    "python XXX.py /ccp_data/inputFile.csv [[ARGS]]",
                    "cp -f result.csv /ccp_data/"
                ]
            }
        ]
    }
}
```

download implementation:
the docker image contains the download script
The following script handles token retrieval and file download:

```sh 
echo "Getting token"
TOKEN=$(curl -X POST $ccpiamurl -d grant_type=refresh_token -d client_id=$ccpclientid -d refresh_token=$ccprefreshtoken -H "X-D4Science-Context: $ccpcontext" | jq -r '."access_token"')

echo Downloading /ccp_data/inputFile.csv
curl $1 -o /ccp_data/inputFile.csv -H "Authorization: Bearer $TOKEN"

echo "Downloaded"
```

It can be downloaded from the following URL: [download.sh](../_static/support_files/download.sh)

example: [Ariadne Dutch Archaeology Named Entity Recognizer](../methods_ccp/Ariadne%20Dutch%20Archaeology%20Named%20Entity%20Recognizer-1.0.0.json)