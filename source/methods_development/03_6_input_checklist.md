# Checklist

The inputs of type checklist allows to define a checklist for single or multiple choise

![checklist](../_static/imgs/methods_development/input_checklist.png )

```json
{
    "inputs": {
        "annotations": {
            "id": "annotations",
            "title": "Annotations",
            "description": "The requested annotations",
            "minOccurs": 1,
            "maxOccurs": 1,
            "schema": {
                "type": "string",
                "format": "checklist",
                "contentMediaType": "text/plain",
                "default": ":Abuse,:Topic,:Politician,:Party,:Hashtag,:UserID,:URL,:Person,:Location,:Organization,:Date,:Address,:Money,:Percent",
                "enum": [
                    ":Abuse",
                    ":Topic",
                    ":Politician",
                    ":Party",
                    ":Hashtag",
                    ":UserID",
                    ":URL",
                    ":Person",
                    ":Location",
                    ":Organization",
                    ":Date",
                    ":Address",
                    ":Money",
                    ":Percent",
                    ":Token",
                    ":SpaceToken",
                    ":Sentence",
                    ":AbusePhrase",
                    ":SentenceSentiment",
                    ":Tweet",
                    ":TweetSegment",
                    ":Author",
                    ":ReplyTo",
                    ":RetweetedAuthor",
                    ":SlurLookup",
                    ":OffensiveLookup",
                    ":SensitiveLookup"
                ]
            }
        }
    },
     "additionalParameters": {
        "parameters": [
            {
                "name": "deploy-script",
                "value": [
                    "echo {{file}} | base64 -d > /ccp_data/input"
                ]
            },
            {
                "name": "execute-script",
                "value": [
                    "wget {{baseurl}}/{{service}}?annotations={{annotations}} --post-file /ccp_data/input --header \"Authorization: Basic {{credentials}}\" --header \"Content-Type: {{contenttype}}\" --header \"Accept: application/json\" -O /ccp_data/annotated.json"
                ]
            },
            {
                "name": "undeploy-script",
                "value": []
            }
        ]
    },
}
```

example: [Gate Cloud Hate And Abuse Detection ](../methods_ccp/Gate%20Cloud%20Hate%20And%20Abuse%20Detection-1.0.0.json)
