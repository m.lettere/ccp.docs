# Input File

the input with format "file" allows the user to upload a file while executing the method

![input file](../_static/imgs/methods_development/input_file.png)

file size is limited to 100Kb, because the content of the file is base64 encoded and passed as a variable

```json
{
    "inputs": {
        "file": {
            "id": "file",
            "title": "Input file",
            "description": "The input file to be annotated",
            "minOccurs": 1,
            "maxOccurs": 1,
            "schema": {
                "type": "string",
                "format": "file",
                "contentMediaType": "text/plain",
                "default": ""
            }
        }
    }
}
```

to access the file content, it's possible to decode it in the deploy script, using a comand like

```sh
echo {{file}} | base64 -d > /ccp_data/input
```

then simply read the file  `/ccp_data/input` during the method execution

```json
{
    "additionalParameters": {
        "parameters": [
            {
                "name": "deploy-script",
                "value": [
                    "echo {{file}} | base64 -d > /ccp_data/input"
                ]
            },
            {
                "name": "execute-script",
                "value": [
                    "wget {{baseurl}}/{{service}}?annotations={{annotations}} --post-file /ccp_data/input  -O /ccp_data/annotated.json"
                ]
            }
        }
    }
}
```

to work with bigger files, refer to the [Input file workspace section](./03_3_input_file_workspace_public.md)

example: [Python example](../methods_examples/Python%20example-1.0.0.json)
