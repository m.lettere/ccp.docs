# Input URL

The input with format "url" allows the user to provide a direct link to a file or resource to be processed during the method execution.

![input url](../_static/imgs/methods_development/input_url.png)

```json
{
    "inputs": {
        "file_url": {
            "id": "file_url",
            "title": "Input URL",
            "description": "The URL of the file to be processed",
            "minOccurs": 1,
            "maxOccurs": 1,
            "schema": {
                "type": "string",
                "format": "url",
                "contentMediaType": "text/plain",
                "default": ""
            }
        }
    }
}
```

The input URL must point to a publicly accessible resource, as the method execution will fetch the content from the URL during runtime.

To process the file content, the provided URL has to be retrieved in the deploy script using a command like:

`wget {{file_url}} -O /ccp_data/input`

or 

`curl -L {{file_url}} -o /ccp_data/input`

Once the file is retrieved, the method can read it from the /ccp_data/input path during execution.

```json
{
    "additionalParameters": {
        "parameters": [
            {
                "name": "deploy-script",
                "value": [
                    "wget {{file_url}} -O /ccp_data/input"
                ]
            },
            {
                "name": "execute-script",
                "value": [
                    "wget {{baseurl}}/{{service}} --post-file /ccp_data/input  -O /ccp_data/annotated.json"
                ]
            }
        ]
    }
}
```

Example: [WorldCloud Urls](../methods_examples/WordCloud%20Urls-1.0.0.json)

