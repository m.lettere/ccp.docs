# Download Public File from Workspace

To retrieve a file from the workspace using its public link, you can handle it just like any [generic URL input](./03_2_input_urls.md).

Make sure to use the `-L` option with the `curl` command to ensure that any HTTP redirects are followed.

## Method JSON

```json
{
    "inputs": {
        "inputFile": {
            "id": "inputFile",
            "title": "inputFile",
            "description": "Input CSV file()",
            "minOccurs": 1,
            "maxOccurs": 1,
            "schema": {
                "type": "string",
                "format": "remotefile",
                "default": "",
                "contentMediaType": "text/csv"
            }
        }
    },
    "additionalParameters": {
        "parameters": [
            {
                "name": "deploy-script",
                "value": [
                    "curl -L {{inputFile}} -o /ccp_data/inputFile.csv"
                ]
            },
            {
                "name": "execute-script",
                "value": [
                    "python XXX.py /ccp_data/inputFile.csv [[ARGS]]",
                    "cp -f result.csv /ccp_data/"
                ]
            }
        ]
    }
}
```

Example: [WorldCloud Urls](../methods_examples/WordCloud%20Urls-1.0.0.json)
