# Credentials and Remote authentication

The `credentials` input allows for the provision of Basic authentication credentials required to access external services. This input is configured as a `secret` field to ensure that sensitive data is handled securely during interactions with the remote service.

![credentials](../_static/imgs/methods_development/input_credentials.png)

```json
{
    "inputs": {
        "credentials": {
            "id": "credentials",
            "title": "Credentials",
            "description": "The Basic auth credentials",
            "minOccurs": 1,
            "maxOccurs": 1,
            "schema": {
                "type": "string",
                "format": "secret",
                "contentMediaType": "text/plain",
                "default": "Z2M4YmEyZDQyaWxiOjNsdWF6dXFhcnE2NW5wZWlwamwy"
            }
        }
    },
    "additionalParameters": {
        "parameters": [
            {
                "name": "execute-script",
                "value": [
                    "wget {{baseurl}}/{{service}}?annotations={{annotations}} --post-file /ccp_data/input --header \"Authorization: Basic {{credentials}}\" --header \"Content-Type: {{contenttype}}\" --header \"Accept: application/json\" -O /ccp_data/annotated.json"
                ]
            }
        ]
    }
}
```

