# Experimentation vs Consolidation


## Experimentation

**image**: python 3.8

**scripts_deploy**:

```sh
mkdir /ccp_data/output
git clone {{ repository }} execution
cd execution
pip install -r requirements.txt
cd -
```

## Consolidation


**image**: hub.dev.d4science.org/ccp-runtimes/gatecloud-base:latest

**scripts_deploy**:

```sh

./download.sh {{ inputFile }}

```