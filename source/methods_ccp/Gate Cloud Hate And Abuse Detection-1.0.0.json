{
    "title": "Gate Cloud Hate And Abuse Detection",
    "description": "A service that tags abusive utterances in any text with the type of abuse (sexist, racist, etc.) and whether the abuse was aimed at the addressee or some other party. This can be run on any English language text. You can check also which words or phrases were deemed potentially abusive via the SlurLookup, SensitiveLookup and OffensiveLookup output options - for full details see https://cloud.gate.ac.uk/shopfront/displayItem/gate-hate-generic",
    "version": "1.0.0",
    "jobControlOptions": "async-execute",
    "metadata": [
        {
            "title": "Marco Lettere",
            "role": "author",
            "href": "https://accounts.d4science.org/auth/admin/realms/d4science/users/09138708-9a19-4724-93d1-8c721d591da2"
        },
        {
            "role": "category",
            "title": "Gate Cloud"
        }
    ],
    "inputs": {
        "ccpimage": {
            "id": "ccpimage",
            "title": "Runtime",
            "description": "The image of the runtime to use for method execution. This depends on the infrastructure specific protocol for interacting with registries.",
            "minOccurs": 1,
            "maxOccurs": 1,
            "schema": {
                "type": "string",
                "format": "url",
                "contentMediaType": "text/plain",
                "default": "bash",
                "readOnly": true
            }
        },
        "service": {
            "id": "service",
            "title": "Service name",
            "description": "The name of the Gate Cloud service",
            "minOccurs": 1,
            "maxOccurs": 1,
            "schema": {
                "type": "string",
                "format": null,
                "contentMediaType": "text/plain",
                "default": "gate-hate-generic",
                "readOnly": true
            }
        },
        "baseurl": {
            "id": "baseurl",
            "title": "Base URL",
            "description": "The base URL of the Gate Cloud REST API",
            "minOccurs": 1,
            "maxOccurs": 1,
            "schema": {
                "type": "string",
                "format": "url",
                "contentMediaType": "text/plain",
                "default": "https://cloud-api.gate.ac.uk/process/",
                "readOnly": true
            }
        },
        "file": {
            "id": "file",
            "title": "Input file",
            "description": "The input file to be annotated",
            "minOccurs": 1,
            "maxOccurs": 1,
            "schema": {
                "type": "string",
                "format": "file",
                "contentMediaType": "text/plain",
                "default": ""
            }
        },
        "contenttype": {
            "id": "contenttype",
            "title": "Content type",
            "description": "The content type of the input file",
            "minOccurs": 1,
            "maxOccurs": 1,
            "schema": {
                "type": "string",
                "format": null,
                "contentMediaType": "text/plain",
                "default": "text/plain",
                "enum": [
                    "text/plain",
                    "application/pdf",
                    "text/html"
                ]
            }
        },
        "credentials": {
            "id": "credentials",
            "title": "Credentials",
            "description": "The Basic auth credentials",
            "minOccurs": 1,
            "maxOccurs": 1,
            "schema": {
                "type": "string",
                "format": "secret",
                "contentMediaType": "text/plain",
                "default": "Z2M4YmEyZDQyaWxiOjNsdWF6dXFhcnE2NW5wZWlwamwy"
            }
        },
        "annotations": {
            "id": "annotations",
            "title": "Annotations",
            "description": "The requested annotations",
            "minOccurs": 1,
            "maxOccurs": 1,
            "schema": {
                "type": "string",
                "format": "checklist",
                "contentMediaType": "text/plain",
                "default": ":Abuse,:Topic,:Politician,:Party,:Hashtag,:UserID,:URL,:Person,:Location,:Organization,:Date,:Address,:Money,:Percent",
                "enum": [
                    ":Abuse",
                    ":Topic",
                    ":Politician",
                    ":Party",
                    ":Hashtag",
                    ":UserID",
                    ":URL",
                    ":Person",
                    ":Location",
                    ":Organization",
                    ":Date",
                    ":Address",
                    ":Money",
                    ":Percent",
                    ":Token",
                    ":SpaceToken",
                    ":Sentence",
                    ":AbusePhrase",
                    ":SentenceSentiment",
                    ":Tweet",
                    ":TweetSegment",
                    ":Author",
                    ":ReplyTo",
                    ":RetweetedAuthor",
                    ":SlurLookup",
                    ":OffensiveLookup",
                    ":SensitiveLookup"
                ]
            }
        }
    },
    "outputs": {},
    "additionalParameters": {
        "parameters": [
            {
                "name": "deploy-script",
                "value": [
                    "echo {{file}} | base64 -d > /ccp_data/input"
                ]
            },
            {
                "name": "execute-script",
                "value": [
                    "wget {{baseurl}}/{{service}}?annotations={{annotations}} --post-file /ccp_data/input --header \"Authorization: Basic {{credentials}}\" --header \"Content-Type: {{contenttype}}\" --header \"Accept: application/json\" -O /ccp_data/annotated.json"
                ]
            },
            {
                "name": "undeploy-script",
                "value": []
            }
        ]
    },
    "links": [
        {
            "rel": "compatibleWith",
            "title": "D4Science development Infrastructure",
            "href": "infrastructures/d4science-dev-swarm"
        },
        {
            "rel": "compatibleWith",
            "title": "D4Science production Infrastructure",
            "href": "infrastructures/d4science-prod-swarm"
        }
    ],
    "keywords": [
        "gatecloud",
        "hate",
        "abuse"
    ],
    "id": "efc83b55-3b88-493f-8f80-f8acb8bec9b2"
}
