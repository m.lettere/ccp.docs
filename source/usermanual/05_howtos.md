

# How To 

## How to use `ccptaskid` to separate output of different replicas to different files in `execute-script`.

``` bash
    mkdir -p /ccp_data/output && echo $RANDOM >> /ccp_data/output/`printenv ccptaskid`.txt
``` 

* **ccpiamurl** is the URL of the Identity management service.
* **ccpclientid** is the client_id to be used for requesting token renewal.
* **ccprefreshtoken** is a refresh token by which new access tokens can be requested.
* **ccpcontext** represents the context (VO or VRE) in which the Method execution has been requested.

As an example the following Python code shows how to use the variables to request a token renewal.

*How to request a login token and an UMA token for accessing D4Science service from inside Method code*
```python
#Get auth info from env variables
refreshtoken = os.environ["ccprefreshtoken"]
context = os.environ["ccpcontext"]
clientid = os.environ["ccpclientid"]
iam = os.environ["ccpiamurl"]

#Auth related functions
logindata = { 'grant_type' : 'refresh_token', 'client_id' : clientid, 'refresh_token' : refreshtoken}
loginheaders = { "Accept" : "application/json", "Content-Type" : "application/x-www-form-urlencoded"}

umadata = { 'grant_type' : 'urn:ietf:params:oauth:grant-type:uma-ticket', 'audience' : context}
umaheaders = { "Accept" : "application/json", "Content-Type" : "application/x-www-form-urlencoded"}

def getToken():
    # login with offline_token
    resp1 = requests.post(iam, data=logindata, headers=loginheaders)
    jwt = resp1.json()
    #get UMA token for context
    umaheaders["Authorization"] = "Bearer " + jwt["access_token"]
    resp2 = requests.post(iam, data=umadata, headers=umaheaders)
    return resp2.json()["access_token"]

# Get valid token for context
tok = getToken()

# List VRE fodler content
vrefolder = requests.get(workspace + "/vrefolder", headers={"Accept" : "application/json", "Authorization" : "Bearer " + tok}).json()s
```

A similar example in bash could have the following form:

### How to download a file from the D4Science workspace from a private URL passed as input parameter

```bash    
echo "Getting token"
TOKEN=$(curl -X POST $ccpiamurl -d grant_type=refresh_token -d client_id=$ccpclientid -d refresh_token=$ccprefreshtoken -H "X-D4Science-Context: $ccpcontext" | jq -r '."access_token"')

echo Downloading $wslink to $inputfile
curl -L $wslink -o $inputfile -H "Authorization: Bearer $TOKEN"

echo "Downloaded"
```

A special folder is provided in the Runtime to a Method execution for storing output files. Files are currently the only way for a Method to output results by value. The folder is named **/ccp_data** and all the files written to this folder are returned in the context of the Execution as a zip archive.
