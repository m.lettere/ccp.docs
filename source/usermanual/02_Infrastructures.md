# Infrastructures 

Infrastructures are technological platforms on top of whom Methods are executed. They are identified by a unique id, a name, a description and a type.

The type defines also the encapsulation or containerisation technology used for the runtimes.

Currently CCP supports single Docker containers, Docker Swarm based clusters and LXD clusters.

It is responisibility of a Method developers to decide what infrastructure their Methods will be executed on and this restricts the type of Runtimes that can be selected. 