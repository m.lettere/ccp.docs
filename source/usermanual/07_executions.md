# Executions

An **Execution** represents the instatiation of a Method through a Request. The Request carries values for the inputs declared by the Method and a list of expected outputs. A dedicated data structure is created in the Execution repository as soon as an Execution request is accepted. The data structure acts as a folder that collects the request, all the outputs and all metadata generated in order to execute a Method.

An Execution can be exported, imported and re-executed through the CCP GUI widgets or through REST API calls.

## Anatomy of an Execution

The datastructure of an Execution is meant to be as atomic and as explicative as possible.
The following is a representation of the data structure representing an Execution.

* **metadata**
  * **request.json**          # The JSON message that requested the Execution
  * **method.json**           # The JSON Method descriptor
  * **infrastructure.json**   # The JSON Infrastructure descriptor
  * **instance.json**         # The JSON descriptor of the container that played as the Runtime of the Execution
* **auth**
  * **jwt.json**              # Authorization information of the user requesting the Execution
* **outputs**
  * **output.zip**            # Zip archive of all output files and folders.


## Method and Execution storage

In order to be able to execute a Method or to operate on an Execution they need to be kept inside the **Workbench**. The Workbench can be seen as a sort of short lived storage area that resides closely to the CCP core components.

A Method that is on the Workbench can be cloned, edited, executed, deleted, exported or archived to a offline storage area. The offline storage area is a dedicated folder CCP/methods on a user's D4Science workspace. When exported to file or archive to the worksapce a MEthod is a JSON file named like the Method's title and version. 

An Execution that is on the workbench can be browsed, downloaded, re-executed, deleted or archived to an offline storage area which resides in a dedicated folder CCP/executions on a user's workspace. When downloaded to a file or archived to the workspace, an Execution is a zip archive containing a compressed binary with the structure described in the previous section.

Methods and Executions can be reimported to the Workbench Beither by uploading exported files or passing the "sharable links" obtained from the workspace. 
