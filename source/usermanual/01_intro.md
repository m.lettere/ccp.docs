# User manual

This manual documents the features of CCP (Cloud Computing Platform) available to scientists that want to design, integrate and execute their methods.