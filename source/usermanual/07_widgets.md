# UI Widgets

A set of graphical user interaction (GUI) widgets are provided in order to allow a user to interact from browser based applications with Methods and Executions stored in the Workbench.

## Method list

The *Method list* widget is a visual representantion of the list of Methods that a user is able to access in a given context either because he/she is the owner or because they are shared in the context.

The following Figure shows an example visualization of the Method list.

.. figure:: /images/methodlistwidget.png
    :alt: Method list widget

    Method list widget

The Method list widget is comprised of a toolbar, a search field and the list of Methods. The Methods are organized by categories as shown in [1]. For every Method the title, version, author and description are reported in the first two lines [2]. As additional information tags and compatible Infrastructure are shown [3]. There is the possibility to download a Method or a whole class and to see how many of the shown Methods are executable [4]. Methods can be not executable if their compatible Infrastrucure is not known or available. A per MEthod tollbar [5] allows to download, edit or execute a Method for. From the global toolbar it is possible to refresh the list or upload a Method from a file [6] and also to reimport an archived Method from the workspace by copying and pasting the shareable link into the proper field and clicking on the button [7]. 

## Method editor

The *Method editor* widget is a visual tool for creating, deleting, editing, cloning or deriving a Method descriptor.

The following Figure shows an example visualization of the Method editor.

![Method editor widget](images/methodeditorwidget.png)

From the global toolbar [1] it is possible to save the edited Method or delete it or clear all the form fields. The metadata area [2] contains the controls to define all the metadata of a Method including title, version, description, tags, categories. It is also possible to choose a compatible Infrastructure from the available ones. In the input definition area [3] the user can define all the input parameters with their type, format, encoding, cardinality and default values. In the output definition area [4] the user can define all the output files that can be expected from an Execution with their type, format, encoding and cardinality. Shortcuts are added to define with one click the standard output and error chnannels of a Method. In the scripting area [5] the deploy, execute and undeploy script can be defined. 

Once the Method is saved the user will automatically be added as the Author. If the user defines the method to be public, the Method is made available to all members of the context (VO or VRE) in which the operation occurred.

## Method execution form

The *Method execution form* widget is a visual tool for requesting the execution of a Method.

The following Figure shows an example visualization of the Method execution form.

![Method execution form](images/executionformwidget.png)

In the area labeled with [1], the principal metadata of the Method is shown. In the Inputs area [2] it is possible to edit the input values that are passed to the Execution. The input fields try to match as close as possible the constraints defined for the input in the Method descriptor. The execute button [4] triggers the sending of the execution request. A code generator widget [5] can be used in order to download code snippets that act as working examples for requesting the Execution programmatically through the CCP APIs (see :ref:`rest`). Currently Python 3, R, Jupyter Notebook and Julia are supported but the list will grow in the future.

A direct link is provided that, when navigated to, will open the execution form with exactly the same parametrization that has been submitted for execution.

## Execution monitor

The *Execution Monitor* widget is a visual tool for monitoring Executions.

The following Figure shows an example visualization of the Execution monitor.

![Execution monitor](images/executionmonitorwidget.png)

The list of Executions are organized by their Method names [1]. For every Method the number of executions in the different states ('accepted', 'running', 'successful', 'failed') is reported. When expanding a Method the list of all Executions is shown [2]. Every list item reports information about the Method version, the status, timing, Infrastructure and Runtime of the Execution. When outputs are available a link to download them is shown [3]. Actions that can be performed on an Execution [5] are download as zip archive, archive to workspace, download provenance descriptor in Prov-O format, re-execute and delete. An Execution item can also be drag and dropped onto the Execution form widget in order to prepare a request for a new onne. Code for programmatically requesting the same Execution can be generated with a proper code generation widget [6]. Currently Python 3, R, Julia and Jupyter Notebooks are currently supported. 

A direct link is shown that allows, when navigated to, to open the execution form with exatly the same parametrization of the execution itself. 

By using the triggers on the global toolbar [6] the list of Executions can be refreshed, a previously exported Execution can be reimported from a local zip file and archived Exections can be re-imported from their sharable workspace link. 

