echo "Getting token"
TOKEN=$(curl -X POST $ccpiamurl -d grant_type=refresh_token -d client_id=$ccpclientid -d refresh_token=$ccprefreshtoken -H "X-D4Science-Context: $ccpcontext" | jq -r '."access_token"')

echo Downloading /ccp_data/inputFile.csv
curl $1 -o /ccp_data/inputFile.csv -H "Authorization: Bearer $TOKEN"

echo "Downloaded"
