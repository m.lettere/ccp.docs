# Introduction

Cloud Computing Platform (CCP) is a type of service that offers on-demand and scalable availability of computing resources such as servers, storage, and software over the internet. It eliminates the need for the users to manage their own physical infrastructure, while being able to execute, reexecute, demonstrate and share their procedures, algorithms, comuptations and results with their scientific community independently technological and implementation details. 

CCP has many benefits, such as:

* Faster time to science, allowing developers to accelerate development with quick deployments
* Scalability and flexibility, giving more flexibility to adjust computing resources according to the users needs
* Better collaboration, enabling teams to work together from anywhere with an internet connection and access the same data and applications
* Productivity, allowing users to focus on their core science functions and innovation without worrying about the underlying infrastructure or  maintenance
* Performance, delivering high-speed and reliable services with minimal downtime or latency
* Disaster recovery, enabling users to restore methods and applications quickly and easily in the event of a crisis
* Competitiveness, giving users access to the most innovative and cutting-edge technology available and helping them stay ahead of the market trends

CCP is built upon the experience of the previous Dataminer initiative <https://en.wikipedia.org/wiki/D4Science> and uses a novel approach based on containerization, REST APIs and Json.

Several fields of ICT have experienced a major evolution during the last decade and many new advances, such as the widespread adoption of microservice development patterns. This resulted in substantial improvements in terms of interoperability and composability of software artefacts.
The vast landscape of new opportunities, in addition to the greatly increased requirements and expectations, have been the drivers for the design and development of a new Cloud Computing Platform that represents the result of a global rethink of the Data Miner.
