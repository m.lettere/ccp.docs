# ccp.docs

Online docs for CCP

## prerequisites:

* (optionally) create a virtualenv
```sh
python -m venv venv
. ./venv/bin/activate
pip install --upgrade pip
```

install [Sphinx](https://www.sphinx-doc.org/en/master/usage/installation.html)

install pip requirements
`pip install -r requirements.txt`

## compile
if using a venv, activate it 

```sh
. ./venv/bin/activate
```

then 
### windows
make.bat html

### unix/macOs

make html